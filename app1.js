const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const morgan = require("morgan");
const path = require("path");
const expressHBS = require("express-handlebars");
const errorController = require("./controllers/error");
const sequelize = require("./database/database");
const Product = require("./models/product");
const User = require("./models/user");
const Cart = require("./models/cart");
const CartItem = require("./models/cart-item");
const Order = require("./models/order");
const OrderItem = require("./models/order-item");
// const userMiddleware = require("./middleware/user");

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({extended: false}));
app.use(morgan("tiny"));

app.engine("hbs", expressHBS({layoutsDir: "views/layouts", defaultLayout: "main-layout", extname: "hbs", partialsDir: "views/include"}));
app.set("view engine", "hbs");
app.set("views", "views");

app.use((req, res, next) => {
  User.findByPk(1).then(user => {
    console.log("MIDDLEWARE CALL===========", user.id);
    req.user = user;
    next();
  }).catch(err => console.log("Error in USER MIDDLEWARE"));
});

//Routes
app.use("/admin", adminRoutes.routes);
app.use(shopRoutes);

//404 Page Error
app.use(errorController.get404);

//middleware

//database
Product.belongsTo(User, {
  constraints: true,
  onDelete: "CASCADE"
});
User.hasMany(Product);
User.hasOne(Cart);
Cart.belongsTo(User);
Cart.belongsToMany(Product, {through: CartItem});
Product.belongsToMany(Cart, {through: CartItem});
Order.belongsTo(User);
User.hasMany(Order);
Order.belongsToMany(Product, {through: OrderItem});

sequelize
// .sync({force: true})
  .sync().then(result => {
  return User.findByPk(1);
  // console.log(result);
}).then(user => {
  if (!user) {
    return User.create({name: "Kishan", email: "kishan7909.kp@gmail.com"});
  }
  return user;
}).then(user => {
  // console.log(user);
  // return user.createCart();
}).then(cart => {
  app.listen(3000);
}).catch(err => {
  console.log(err);
});

//port
