const Product = require('../models/product')
const mongodb = require('mongodb')
const Brand = require('../models/brands')
const Category = require('../models/categories')

exports.getAddProduct = (req, res, next) => {
  // res.sendFile(path.join(rootpath,'views','add-product.html'))
  // const d = new Date()
  // const random = Math.floor(Math.random() * Math.floor(99999))
  // const pid =
  //   'SHOPSTOCKCL' +
  //   d.getFullYear() +
  //   (d.getMonth() + 1) +
  //   d.getDate() +
  //   random +
  //   d.getSeconds()

  Brand.find()
    .then(brand => {
      Category.find()
        .then(cat => {
          console.log(cat)

          res.render('admin/edit-product', {
            pageTitle: 'Add Product',
            path: 'admin/edit-product',
            activeAddProduct: true,
            brands: brand,
            category: cat,
            editing: false
          })
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))
}
exports.postAddProduct = (req, res, next) => {
  // console.log(req.body)
  // console.log(req.user.id)
  const gst = parseFloat(req.body.gst)
  const cgst = parseFloat(req.body.cgst)
  const totalTax = gst + cgst
  const price = parseFloat(req.body.buyprice)
  const profit = 5000
  const discount = parseFloat(req.body.discount)
  const discountPrice = price * discount / 100
  const tax = price * totalTax / 100
  const sellprice = parseInt(price + profit)

  const id = req.body.productid
  const name = req.body.name
  const category = req.body.category
  const brand = req.body.brand
  const short_detail = req.body.sd
  const full_detail = req.body.fd
  const color = req.body.color
  const image = req.body.uploadimages
  const displayimage = req.body.displayimage
  const qty = req.body.quantity
  const buyprice = req.body.buyprice

  // req.user.createProduct({
  //   name: name,
  //   category: category,
  //   brand: brand,
  //   short_detail: short_detail,
  //   full_detail: full_detail,
  //   color: color,
  //   displayimage: displayimage,
  //   image: image,
  //   discount: discount,
  //   qty: qty,
  //   buyprice: buyprice,
  //   sellprice: sellprice
  // })
  const product = new Product({
    name: name,
    category: category,
    brand: brand,
    sd: short_detail,
    fd: full_detail,
    color: color,
    displayimage: displayimage,
    image: image,
    discount: discount,
    qty: qty,
    buyprice: buyprice,
    sellprice: sellprice,
    userid: req.user._id
  })
  product
    .save()
    .then(result => {
      console.log('NEW PRODUCT CREATED')
      res.redirect('/')
    })
    .catch(err => console.log(err))
  // products.push({title : req.body.title,description: req.body.description})
}

exports.getProducts = (req, res) => {
  Product.find()
    .then(product => {
      res.render('admin/products', {
        prods: product,
        pageTitle: 'All Products',
        path: '/',
        activeAdminProducts: true
      })
    })
    .catch(err => console.log(err))
}

//    Product.fetchAll(products => {
//      res.render("admin/products", {
//        prods: products,
//        pageTitle: "Admin Products",
//        path: "/",
//        activeAdminProducts: true
//      });
//    });
// };

exports.getEditProduct = (req, res) => {
  const editMode = req.query.edit
  console.log(req.params.id)
  if (!editMode) {
    return res.redirect('/')
  }
  // req.user
  //   .getProducts({
  //     where: {
  //       productid: req.params.id
  //     }
  //   })
  Product.findById(req.params.id)
    .then(products => {
      if (!products) {
        return res.redirect('/')
      }
      Brand.find()
        .then(brand => {
          Category.find()
            .then(cat => {
              res.render('admin/edit-product', {
                pageTitle: 'Edit Product',
                path: 'admin/edit-product',
                activeEditProduct: true,
                brands: brand,
                category: cat,
                product: products,
                editing: req.query.edit === 'true' ? true : false,
                productid: req.params.id
              })
            })
            .catch(err => console.log(err))
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))

  //    res.render("admin/edit-product");
}

exports.postEditProduct = (req, res) => {
  console.log(req.body)
  const price = parseFloat(req.body.buyprice)
  const profit = 5000
  const sellprice = parseInt(price + profit)

  const id = req.body.productid
  const name = req.body.name
  const category = req.body.category
  const brand = req.body.brand
  const sd = req.body.sd
  const fd = req.body.fd
  const color = req.body.color
  const image = req.body.uploadimages
  const displayimage = req.body.displayimage
  const discount = parseFloat(req.body.discount)
  const qty = req.body.quantity
  const buyprice = req.body.buyprice

  //   Product.findByPk(id).then(product => {
  //     (product.name = name),
  //     (product.category = category),
  //     (product.brand = brand),
  //     (product.short_detail = short_detail),
  //     (product.full_detail = full_detail),
  //     (product.color = color),
  //     (product.displayimage = displayimage),
  //     (product.image = image),
  //     (product.discount = discount),
  //     (product.qty = qty),
  //     (product.buyprice = buyprice),
  //     (product.sellprice = sellprice);
  //     return product.save();
  //   }).then(result => {
  //     console.log("UPDATED PRODUCT");
  //     res.redirect("/admin/products");
  //   }).catch(err => console.log(err));

  const updateProduct = {
    name,
    category,
    brand,
    sd,
    fd,
    color,
    displayimage,
    image,
    discount,
    qty,
    buyprice,
    sellprice
  }
  Product.findByIdAndUpdate(id, { $set: updateProduct }, { new: true })
    .then(result => {
      res.redirect('/admin/products')
    })
    .catch(err => console.log(err))
}

exports.getdeleteProduct = (req, res) => {
  const id = req.params.id
  Product.findByIdAndRemove(id)
    .then(result => {
      res.redirect('/admin/products')
    })
    .catch(err => console.log(err))
  // console.log(id);
  // Product.findByPk(id).then(product => {
  //   return product.destroy();
  // }).then("PRODUCT DELETED").catch(err => console.log(err));
  // res.redirect("/admin/products");
  //  Product.deleteByProduct(id).then(result => {
  //    res.redirect("/admin/products");
  //  }).catch(err => {
  //    console.log(err);
  //  });
}

exports.postAddBrand = (req, res) => {
  console.log(req.body)
  const brand = new Brand({
    brand_name: req.body.addbrand
  })
  brand
    .save()
    .then(result => {
      console.log(result)
      res.redirect('/admin/products')
    })
    .catch(err => console.log(err))
}

exports.postAddCategory = (req, res) => {
  console.log(req.body)
  const category = new Category({
    category_name: req.body.addcategory
  })
  category
    .save()
    .then(category => {
      res.redirect('/admin/products')
    })
    .catch(err => console.log(err))
}
