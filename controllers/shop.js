const Product = require('../models/product')
const User = require('../models/user')
// const Cart = require("../models/cart");
const Category = require('../models/categories')
const Order = require('../models/Order')

exports.getProducts = (req, res, next) => {
  const searchitem = 'all'
  Product.find()
    .then(product => {
      Category.find()
        .then(cat => {
          const count = req.user.cart.items.length

          res.render('shop/product-list', {
            prods: product,
            pageTitle: 'All Products',
            path: '/products',
            cartitems: count,
            activeProducts: true,
            category: cat,
            search: searchitem
          })
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))

  // Product.fetchAll().then(([rows, fieldDate]) => {
  //   console.log(rows);
  // const count = req.user.cart.items.length//
  // res.render("shop/product-list", {
  // //     prods: rows,
  // cartitems: count,
  // //     pageTitle: "All Products",
  //     path: "/",
  //     activeProducts: true
  //   });
  // }).catch(err => console.log(err));
  // res.sendFile(path.join(rootpath,'views','shop.html'))
}

exports.getIndex = (req, res) => {
  console.log(req.user)
  const count = req.user.cart.items.length

  res.render('shop/index', {
    pageTitle: 'All Products',
    cartitems: count,
    path: '/',

    activeProducts: true
  })
  // Product.findAll().then(product => {
  // const count = req.user.cart.items.length//
  // res.render("shop/index", {
  // //     prods: product,
  // cartitems: count,
  // //     pageTitle: "All Products",
  //     path: "/",
  //     activeProducts: true
  //   });
  // }).catch(err => console.log(err));
}

exports.getCart = (req, res) => {
  req.user
    .populate('cart.items.productId')
    .execPopulate()
    .then(user => {
      console.log(user.cart.items)
      const products = user.cart.items
      const count = req.user.cart.items.length
      res.render('shop/cart', {
        pageTitle: 'Cart',
        cartitems: count,
        path: '/cart',
        activeCart: true,
        products: products,
        // total: total,
        grandTotal: 0
      })

      // return cart
      //   .getProducts()
      //   .then(products => {
      // const count = req.user.cart.items.length//
      // res.render('shop/cart', {
      // //       pageTitle: 'Cart',
      // cartitems: count,
      // //       path: '/cart',
      //       activeCart: true,
      //       products: products
      //       // total: total,
      //       // grandTotal: grandTotal
      //     })
      //   })
      //   .catch(err => console.log('Error in getCart', err))
    })
    .catch(err => console.log(err))

  // Cart.getCart(cart => {
  //   Product.fetchAll(products => {
  //     const cartsProducts = [];
  //     const total = cart.totalPrice;
  //     let grandTotal = 0;
  //     for (product of products) {
  //       const cartProductData = cart.products.find(prod => prod.id === product.productid);
  //       if (cartProductData) {
  //         const totalPrice = product.sellprice * cartProductData.qty;
  //         grandTotal = grandTotal + totalPrice;
  //         cartsProducts.push({productData: product, qty: cartProductData.qty, total: totalPrice});
  //       }
  //     }
  // const count = req.user.cart.items.length//
  // res.render("shop/cart", {
  // //       pageTitle: "Cart",
  // cartitems: count,
  // //       path: "/cart",
  //       activeCart: true,
  //       products: cartsProducts,
  //       total: total,
  //       grandTotal: grandTotal
  //     });
  //   });
  // });
}

exports.postCart = (req, res) => {
  const productid = req.body.productid
  Product.findById(productid)
    .then(product => {
      return req.user.addToCart(product)
    })
    .then(result => {
      console.log(result)
      res.redirect('/cart')
    })
    .catch(err => {
      console.log(err)
    })
  // let fetchCart
  // let newQuantity = 1
  // req.user
  //   .getCart()
  //   .then(cart => {
  //     fetchCart = cart
  //     return cart.getProducts({
  //       where: {
  //         productid: productid
  //       }
  //     })
  //   })
  //   .then(products => {
  //     let product
  //     if (products.length > 0) {
  //       product = products[0]
  //     }

  //     if (product) {
  //       const oldqty = product.cartItem.quantity
  //       newQuantity = oldqty + 1
  //       return product
  //     }
  //     return Product.findByPk(productid)
  //   })
  //   .then(product => {
  //     return fetchCart.addProduct(product, {
  //       through: {
  //         quantity: newQuantity
  //       }
  //     })
  //   })
  //   .then(() => {
  //     res.redirect('/cart')
  //   })
  //   .catch(err => console.log(err))
  // console.log(productid);
  // Product.findById(productid, product => {
  //   Cart.addProduct(productid, parseInt(product.sellprice));
  // });
}

exports.getCheckout = (req, res) => {
  const count = req.user.cart.items.length
  res.render('shop/checkout', {
    pageTitle: 'Checkout',
    cartitems: count,
    path: '/checkout',
    activeCart: true
  })
}

exports.getOrders = (req, res) => {
  // User.getOrders()
  Order.find({ 'user.userId': req.user._id })
    .then(orders => {
      console.log('ORDERS', orders)
      const count = req.user.cart.items.length
      res.render('shop/orders', {
        pageTitle: 'Orders',
        cartitems: count,
        path: '/orders',
        activeOrders: true,
        orders: orders
      })
    })
    .catch(err => console.log(err))
}

exports.postCreateOrders = (req, res) => {
  console.log('Create Order Call')
  req.user
    .populate('cart.items.productId')
    .execPopulate()
    .then(user => {
      console.log(user.cart.items)
      const products = user.cart.items.map(i => {
        return { quantity: i.quantity, product: { ...i.productId._doc } }
      })
      const order = new Order({
        user: {
          name: req.user.name,
          userId: req.user._id
        },
        products: products
      })
      return order.save()
    })
    .then(result => {
      return req.user.clearCart()
    })
    .then(r => {
      res.redirect('/orders')
    })
    .catch(err => console.log(err))
}

exports.getProductsDetails = (req, res) => {
  const productid = req.params.id
  console.log(productid)

  Product.findById(productid)
    .then(product => {
      const count = req.user.cart.items.length
      res.render('shop/product-detail', {
        product: product,
        cartitems: count,
        pageTitle: 'Product Detail',
        path: '/products',
        activeProducts: true
      })
    })
    .catch(err => console.log(err))

  // Product.findById(productid).then(([rows, fieldDate]) => {
  // const count = req.user.cart.items.length//
  // res.render("shop/product-detail", {
  // //      finalprice: rows.sellprice + 800
  // cartitems: count,0,
  // //     prods: rows,
  //     pageTitle: "Product Detail",
  //     path: "/products",
  //     activeProducts: true
  //   });
  // }).catch(err => console.log(err));
  // Product.findById(productid, product => {
  //   res.render("shop/product-detail", {
  //     finalprice: product.sellprice + 8000,
  //     product: product,
  //     pageTitle: "Product Detail",
  //     path: "/products",
  //     activeProducts: true
  //   });
  // });
}

exports.getDeleteCartItem = (req, res) => {
  console.log('DELETE CART ITEM ID', req.params.id)
  console.log(req.user)
  req.user
    .removeCart(req.params.id)
    .then(result => {
      console.log(result)
      res.redirect('/cart')
    })
    .catch(err => console.log(err))
  // req.user
  //   .getCart()
  //   .then(cart => {
  //     return cart.getProducts({
  //       where: {
  //         productid: req.params.id
  //       }
  //     })
  //   })
  //   .then(products => {
  //     const product = products[0]
  //     return product.cartItem.destroy()
  //   })
  //   .then(result => {
  //     res.redirect('/cart')
  //   })
  //   .catch(err => console.log(err))
}

exports.getSearchProducts = (req, res) => {
  const searchitem = req.params.name
  if (searchitem === 'all') {
    Product.find()
      .then(product => {
        Category.find()
          .then(cat => {
            const count = req.user.cart.items.length

            res.render('shop/product-list', {
              prods: product,
              pageTitle: 'All Products',
              path: '/products',
              cartitems: count,
              activeProducts: true,
              category: cat,
              search: searchitem
            })
          })
          .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
  } else {
    return Product.find({ category: searchitem })
      .then(product => {
        Category.find()
          .then(cat => {
            const count = req.user.cart.items.length

            res.render('shop/product-list', {
              prods: product,
              pageTitle: 'All Products',
              path: '/products',
              cartitems: count,
              activeProducts: true,
              category: cat,
              search: searchitem
            })
          })
          .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
  }
}
