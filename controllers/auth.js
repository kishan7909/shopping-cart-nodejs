exports.getLoginPage = (req, res) => {
  res.render('auth/login', {
    pageTitle: 'Add Product',
    path: '/login',
    activeLogin: true,
    navtheme: 'navbar-themed'
  })
}
