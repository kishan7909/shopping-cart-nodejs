exports.get404 = (req, res, next) => {
  // res.status(404).sendFile(path.join(__dirname,'views','404.hbs'))
  res.status(404).render("layouts/404", {pageTitle: "Page Not Found"});
};
