const mongoose = require('mongoose')
const Product = require('./product')
const Schema = mongoose.Schema
const userSchema = Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    index: { unique: true },
    required: true
  },
  cart: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true
        },
        quantity: { type: Number, required: true }
      }
    ]
  }
})

userSchema.methods.removeCart = function(productid) {
  const updatedCart = this.cart.items.filter(p => {
    return p.productId.toString() !== productid.toString()
  })
  this.cart.items = updatedCart
  return this.save()
}

userSchema.methods.clearCart = function() {
  this.cart = { items: [] }
  return this.save()
}

userSchema.methods.addToCart = function(product) {
  const cartProductsIndex = this.cart.items.findIndex(cp => {
    return cp.productId.toString() === product._id.toString()
  })
  let qty = 1
  let updateCartItems = [...this.cart.items]
  if (cartProductsIndex >= 0) {
    qty = this.cart.items[cartProductsIndex].quantity + 1
    updateCartItems[cartProductsIndex].quantity = qty
  } else {
    updateCartItems.push({
      productId: product._id,
      quantity: qty
    })
  }

  const updatedCart = {
    items: updateCartItems
  }
  this.cart = updatedCart
  return this.save()
}

module.exports = mongoose.model('User', userSchema)
//=============================Sequelize=========================================
// const Sequelize = require("sequelize");
// const sequelize = require("../database/database");

// const User = sequelize.define("user", {
//   id: {
//     type: Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true
//   },
//   name: {
//     type: Sequelize.STRING,
//     allowNull: false
//   },
//   email: {
//     type: Sequelize.STRING,
//     allowNull: false
//   }
// });

//================== mongodb =====================================================================================

// const mongodb = require('mongodb')
// const getdb = require('../database/database').getDb
// const md5 = require('md5')

// class User {
//   constructor(username, email, password, cart, id) {
//     ;(this.name = username),
//       (this.email = email),
//       (this.password = md5(password)),
//       (this.cart = cart),
//       (this._id = id)
//   }

//   save() {
//     const db = getdb()
//     return db.collection('users').insertOne(this)
//   }
//   static findbyId(id) {
//     const db = getdb()
//     return db
//       .collection('users')
//       .find({ _id: mongodb.ObjectId(id) })
//       .next()
//   }

//   getCart() {
//     const db = getdb()
//     const productIds = this.cart.items.map(i => {
//       return i.product_id
//     })
//     return db
//       .collection('products')
//       .find({ _id: { $in: productIds } })
//       .toArray()
//       .then(products => {
//         return products.map(p => {
//           return {
//             ...p,
//             quantity: this.cart.items.find(i => {
//               return i.product_id.toString() === p._id.toString()
//             }).quantity
//           }
//           //     return {
//           //       ...p,
//           //       quantity: this.cart.items.find(i => {
//           //         return i.product_id.toString() === p._id.toString()
//           //       }).quantity
//           //     }
//         })
//       })
//   }

//   deleteCartItem(id) {
//     const db = getdb()
//     const updatedCart = this.cart.items.filter(p => {
//       return p.product_id.toString() !== id
//     })
//     console.log(updatedCart)
//     this.cart.items = updatedCart
//     return db
//       .collection('users')
//       .updateOne(
//         { _id: mongodb.ObjectId(this._id) },
//         { $set: { cart: this.cart } }
//       )
//   }

//   addToCart(product) {
//     const db = getdb()
//     const cartProductsIndex = this.cart.items.findIndex(cp => {
//       return cp.product_id.toString() === product._id.toString()
//     })
//     let qty = 1
//     let updateCartItems = [...this.cart.items]
//     if (cartProductsIndex >= 0) {
//       qty = this.cart.items[cartProductsIndex].quantity + 1
//       updateCartItems[cartProductsIndex].quantity = qty
//     } else {
//       updateCartItems.push({
//         product_id: mongodb.ObjectId(product._id),
//         quantity: qty
//       })
//     }

//     const updatedCart = {
//       items: updateCartItems
//     }

//     return db
//       .collection('users')
//       .updateOne(
//         { _id: mongodb.ObjectId(this._id) },
//         { $set: { cart: updatedCart } }
//       )
//   }

//   addOrder() {
//     const db = getdb()
//     return this.getCart()
//       .then(products => {
//         const orders = {
//           items: products,
//           user: {
//             _id: mongodb.ObjectId(this._id),
//             username: this.name
//           }
//         }
//         return db.collection('orders').insertOne(orders)
//       })
//       .then(result => {
//         this.cart = { items: [] }
//         return db
//           .collection('users')
//           .updateOne(
//             { _id: mongodb.ObjectId(this._id) },
//             { $set: { cart: { items: [] } } }
//           )
//       })
//       .catch(err => console.log(err))
//   }

//   static getOrders() {
//     const db = getdb()
//     return db
//       .collection('orders')
//       .find()
//       .toArray()
//   }
// }
// module.exports = User

//==================================== mongoose ===========================================================
