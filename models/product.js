//================================== mongoose ====================================================

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const productSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  brand: {
    type: String,
    required: true
  },
  sd: {
    type: String,
    required: true
  },
  fd: {
    type: String,
    required: true
  },
  color: {
    type: Array,
    required: true
  },
  displayimage: {
    type: String,
    required: true
  },
  image: {
    type: Array,
    required: false
  },
  discount: {
    type: Number,
    required: true
  },
  qty: {
    type: Number,
    required: true
  },
  buyprice: {
    type: Number,
    required: true
  },
  sellprice: {
    type: Number,
    required: true
  },
  userid: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
})

module.exports = mongoose.model('Product', productSchema)

//================================== mongoDb method ===============================================
// const getDb = require('../database/database').getDb
// const mongodb = require('mongodb')

// class Product {
//   constructor(
//     name,
//     category,
//     brand,
//     sd,
//     fd,
//     color,
//     displayimage,
//     image,
//     discount,
//     qty,
//     buyprice,
//     sellprice,
//     id,
//     userid
//   ) {
//     ;(this.name = name),
//       (this.category = category),
//       (this.brand = brand),
//       (this.sd = sd),
//       (this.fd = fd),
//       (this.color = color),
//       (this.displayimage = displayimage),
//       (this.image = image),
//       (this.discount = discount),
//       (this.qty = qty),
//       (this.buyprice = buyprice),
//       (this.sellprice = sellprice),
//       (this._id = id ? mongodb.ObjectId(id) : null),
//       (this.userid = userid)
//   }

//   save() {
//     const db = getDb()
//     if (this._id) {
//       return db
//         .collection('products')
//         .updateOne({ _id: this._id }, { $set: this })
//         .then(result => {
//           console.log(result)
//         })
//         .catch(err => console.log(err))
//     } else {
//       return db
//         .collection('products')
//         .insertOne(this)
//         .then(result => {
//           console.log(result)
//         })
//         .catch(err => console.log(err))
//     }
//   }
//   static fetchAll() {
//     const db = getDb()
//     return db
//       .collection('products')
//       .find()
//       .toArray()
//       .then(result => {
//         console.log(result)
//         return result
//       })
//       .catch(err => console.log(err))
//   }
//   static findByid(id) {
//     const db = getDb()
//     return db
//       .collection('products')
//       .find({ _id: mongodb.ObjectId(id) })
//       .next()
//       .then(result => {
//         console.log(result)
//         return result
//       })
//       .catch(err => console.log(err))
//   }
//   static deleteProduct(id) {
//     const db = getDb()
//     return db.collection('products').deleteOne({ _id: mongodb.ObjectId(id) })
//   }
// }

//====================================== Squelize database ===============================================
// // const Sequelize = require("sequelize");
// // const sequelize = require("../database/database");
// const Product = sequelize.define("product", {
//   productid: {
//     type: Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true
//   },
//   name: {
//     type: Sequelize.STRING,
//     allowNull: false
//   },
//   category: {
//     type: Sequelize.STRING,
//     allowNull: false
//   },
//   brand: {
//     type: Sequelize.STRING,
//     allowNull: false
//   },
//   short_detail: {
//     type: Sequelize.TEXT,
//     allowNull: false
//   },
//   full_detail: {
//     type: Sequelize.TEXT,
//     allowNull: true
//   },
//   color: {
//     type: Sequelize.JSON,
//     allowNull: false
//   },
//   displayimage: {
//     type: Sequelize.TEXT,
//     allowNull: false
//   },
//   image: {
//     type: Sequelize.JSON,
//     allowNull: true
//   },
//   discount: {
//     type: Sequelize.DOUBLE,
//     allowNull: false
//   },
//   qty: {
//     type: Sequelize.INTEGER,
//     allowNull: false
//   },
//   buyprice: {
//     type: Sequelize.DOUBLE,
//     allowNull: false
//   },
//   sellprice: {
//     type: Sequelize.DOUBLE,
//     allowNull: false
//   }
// });

// module.exports = Product
