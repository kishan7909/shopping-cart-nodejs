const mongoose = require('mongoose')
const Schema = mongoose.Schema

const brandSchema = new Schema({
  brand_name: {
    type: String,
    index: { unique: true },
    required: true
  }
})

module.exports = mongoose.model('Brand', brandSchema)
