const mongooes = require('mongoose')
const Schema = mongooes.Schema

const orderSchema = Schema({
  products: [
    {
      product: { type: Object, required: true },
      quantity: { type: Number, required: true }
    }
  ],
  user: {
    name: { type: String, required: true },
    userId: { type: Schema.Types.ObjectId, required: true, ref: 'User' }
  }
})

module.exports = mongooes.model('Order', orderSchema)

//==========================Sequelize==============================
// const Sequelize = require("sequelize");
// const sequelize = require("../database/database");

// const Order = sequelize.define("order", {
//   id: {
//     type: Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true
//   }
// });

// module.exports = Order;
