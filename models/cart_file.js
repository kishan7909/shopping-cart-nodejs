// const fs = require("fs");
// const path = require("path");
// const p = path.join(path.dirname(process.mainModule.filename), "data", "cart.json");

// const getCartS = cb => {
//   fs.readFile(p, (err, fileContent) => {
//     const cart = JSON.parse(fileContent);
//     if (err) {
//       cb([]);
//     } else {
//       cb(cart);
//     }
//   });
// };

// module.exports = class Cart {
//   static addProduct(id, productPrice) {
//     fs.readFile(p, (err, fileContent) => {
//       let cart = {
//         products: [],
//         totalPrice: 0
//       };
//       if (!err) {
//         cart = JSON.parse(fileContent);
//       }
//       const existingProductIndex = cart.products.findIndex(prod => prod.id === id);
//       const existingProduct = cart.products[existingProductIndex];

//       let updatedProduct;
//       if (existingProduct) {
//         updatedProduct = {
//           ...existingProduct
//         };
//         updatedProduct.qty = updatedProduct.qty + 1;

//         cart.products = [...cart.products];
//         cart.products[existingProductIndex] = updatedProduct;
//       } else {
//         updatedProduct = {
//           id: id,
//           qty: 1
//         };
//         cart.products = [
//           ...cart.products,
//           updatedProduct
//         ];
//       }
//       cart.totalPrice = cart.totalPrice + productPrice;
//       fs.writeFile(p, JSON.stringify(cart), err => {
//         console.log("ERROR IN CART", err);
//       });
//     });
//   }

//   static deleteProduct(id, productPrice) {
//     fs.readFile(p, (err, fileContent) => {
//       if (err) {
//         return;
//       }
//       const updatedCart = {
//         ...JSON.parse(fileContent)
//       };
//       const product = updatedCart.products.findIndex(prod => prod.productid === id);
//       const productQty = product.qty;
//       updatedCart.products = updatedCart.products.filter(prod => prod.productid !== id);
//       updatedCart.totalPrice = productPrice - productPrice * productQty;
//       fs.writeFile(p, JSON.stringify(updatedCart), err => {
//         console.log("ERROR IN CART", err);
//       });
//     });
//   }

//   static getCart(cb) {
//     fs.readFile(p, (err, fileContent) => {
//       const cart = JSON.parse(fileContent);
//       if (err) {
//         cb([]);
//       } else {
//         cb(cart);
//       }
//     });
//   }

//   static deleteCartItem(id) {
//     getCartS(cart => {
//       const cartitem = cart.products.filter(c => c.id !== id);
//       fs.writeFile(p, JSON.stringify({products: cartitem}), err => {
//         console.log("ERROR IN CART", err);
//       });
//     });
//   }
// };
