// const Sequelize = require("sequelize");

// const sequelize = new Sequelize("node_demo", "root", "", {
//   dialect: "mysql",
//   host: "localhost"
// });

// module.exports = sequelize;
// const mongodb = require("mongodb");
// const MongoClient = mongodb.MongoClient;
const mongoose = require('mongoose')

// let _db
const mongooseConnect = callback => {
  mongoose
    .connect('mongodb://localhost:27017/shopperstock')
    .then(client => {
      console.log('Connected')
      callback()
    })
    .catch(err => {
      console.log('Database connection fail')
      throw err
    })
}

// const mongoConnect = callback => {
//   MongoClient.connect("mongodb://localhost:27017/shopperstock").then(client => {
//     console.log("Connected");
//     _db = client.db();
//     callback();
//   }).catch(err => {
//     console.log("Database connection fail");
//     throw err;
//   });
// };

// const getDb = () => {
//   if (_db) {
//     return _db
//   }
//   throw 'No database found'
// }

exports.mongooseConnect = mongooseConnect
// exports.getDb = getDb
