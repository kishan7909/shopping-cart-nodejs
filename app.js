const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const adminRoutes = require('./routes/admin')
const shopRoutes = require('./routes/shop')
const authRoutes = require('./routes/auth')

const morgan = require('morgan')
const path = require('path')
const mongooseConnect = require('./database/database').mongooseConnect

const errorController = require('./controllers/error')
const User = require('./models/user')

// const userMiddleware = require("./middleware/user");

app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(morgan('tiny'))

app.set('view engine', 'ejs')
app.set('views', 'views')

app.use((req, res, next) => {
  User.findById('5cadc123446ab42bfcaf9a34')
    .then(user => {
      console.log('MIDDLEWARE')
      req.user = user
      next()
    })
    .catch(err => console.log('Error in USER MIDDLEWARE'))
})

//Routes
app.use('/admin', adminRoutes.routes)
app.use(shopRoutes)
app.use(authRoutes)
//404 Page Error
app.use(errorController.get404)

//database
mongooseConnect(() => {
  // const user = new User({
  //   name: 'Kishan Patel',
  //   email: 'ksp@narola.email',
  //   cart: {
  //     items: []
  //   }
  // })
  // user.save()
  app.listen(3000)
})
