const mx = require('laravel-mix')
const fs = require('fs-extra')

!mx.inProduction() && mx.webpackConfig({ devtool: 'source-map' }).sourceMaps()

mx
  .options({ processCssUrls: false })
  .sass('sass/app.scss', 'public/css/app.css')
