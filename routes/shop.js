const path = require('path')
const express = require('express')
const router = express.Router()
const rootpath = require('../helper/path')
const shopController = require('../controllers/shop')
const adminData = require('./admin')

router.get('/', shopController.getIndex)

router.get('/products', shopController.getProducts)
router.get('/products/:id', shopController.getProductsDetails)

router.get('/cart', shopController.getCart)
router.post('/cart', shopController.postCart)

router.get('/orders', shopController.getOrders)
// // router.get("/checkout", shopController.getCheckout);

router.post('/create-orders', shopController.postCreateOrders)

router.get('/cart-delete-item/:id', shopController.getDeleteCartItem)

router.get('/searchproducts/:name', shopController.getSearchProducts)

// router.get('/addnewProduct', (req, res) => {
//   res.redirect('/products')
// })

module.exports = router
