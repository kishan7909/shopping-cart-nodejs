const express = require('express')
const router = express.Router()
const path = require('path')
const rootpath = require('../helper/path')
const adminController = require('../controllers/admin')

//GET
router.get('/add-product', adminController.getAddProduct)
router.post('/add-product', adminController.postAddProduct)

router.post('/add-brand', adminController.postAddBrand)
router.post('/add-category', adminController.postAddCategory)

router.get('/products', adminController.getProducts)

router.get('/edit-product/:id', adminController.getEditProduct)
router.post('/edit-product', adminController.postEditProduct)

router.get('/delete-product/:id', adminController.getdeleteProduct)

exports.routes = router
